<?php

namespace TwostepTest;

use GuzzleHttp\Client as HttpClient;
use Twostep\Client as Client;
use Twostep\Exceptions\APIConnectionException;
use Twostep\Exceptions\ResponseException;
use Twostep\Token;
use Twostep\TokenStore;
use Twostep\Error;


class MyTokenStore extends TokenStore
{
    private $token = null;

    public function load()
    {
        //echo "Loading token from store\n";
        return $this->token;
    }

    public function save($token)
    {
        //echo "Saving token to store\n";
        $this->token = $token;
    }

    public function clear()
    {
        $this->token = null;
    }
}

class ExpiredTokenStore extends TokenStore
{
    public function load()
    {
        $token = new Token();
        $token->accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A";
        $token->tokenType = "Bearer";
        $token->scope = "none";
        $token->expiresIn = 3600;
        $token->refreshToken = "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f";
        return $token;
    }

    public function save($token)
    {
    }

    public function clear()
    {
    }
}

class BadTokenStore extends TokenStore
{
    public function load()
    {
        $token = new Token();
        $token->accessToken = "will.not.work";
        $token->tokenType = "Bearer";
        $token->scope = "none";
        $token->expiresIn = 3600;
        $token->refreshToken = "";
        return $token;
    }

    public function save($token)
    {
    }

    public function clear()
    {
    }
}

class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected $httpClient;
    protected $client;

    protected $clientId;
    protected $clientSecret;

    public function setUp()
    {
        $this->clientId = "12345";
        $this->clientSecret = "67890";

        $this->client = new Client($this->clientId, $this->clientSecret, true);
        $this->client->setTokenStore(new \TwostepTest\MyTokenStore);
    }

    public function testClientWithNoClientId()
    {
        try {
            $cli = new Client("", $this->clientSecret);
            $this->fail();
        } catch (\InvalidArgumentException $e) {
            $this->assertNotNull($e);
        }
    }

    public function testClientWithNoClientSecret()
    {
        try {
            $cli = new Client($this->clientId, "");
            $this->fail();
        } catch (\InvalidArgumentException $e) {
            $this->assertNotNull($e);
        }
    }

    public function testGetUser()
    {
        $user = $this->client->getUser("123456");
        $this->assertNotNull($user);
        // One more time to check token store
        $user2 = $this->client->getUser("123456");
        $this->assertNotNull($user2);
    }

    public function testGetUnknownUser()
    {
        try {
            $user = $this->client->getUser("000000");
            $this->fail();
        } catch (ResponseException $e) {
            $this->assertEquals(404, $e->getStatus());
        }
    }

    public function testCreateUser()
    {
        $user = $this->client->createUser("jdoe@example.com", "+12125551234", 1);
        $this->assertNotNull($user);
    }

    public function testCreateUserInvalidEmail()
    {
        try {
            $user = $this->client->createUser("jdoeexample.com", "+12125551234", 1);
            $this->fail();
        } catch (ResponseException $e) {
            $this->assertEquals(400, $e->getStatus());
        }
    }

    public function testRemoveUser()
    {
        $user = $this->client->removeUser("123456");
        $this->assertNotNull($user);
    }

    public function testRemoveUnknownUser()
    {
        try {
            $user = $this->client->removeUser("000000");
            $this->fail();
        } catch (ResponseException $e) {
            $this->assertEquals(404, $e->getStatus());
        }
    }

    public function testRequestSms()
    {
        $receipt = $this->client->requestSms("123456");
        $this->assertNotNull($receipt);
    }

    public function testRequestSmsUnknownUser()
    {
        try {
            $receipt = $this->client->requestSms("000000");
            $this->fail();
        } catch (ResponseException $e) {
            $this->assertEquals(404, $e->getStatus());
        }
    }

    public function testRequestCall()
    {
        $receipt = $this->client->requestCall("123456");
        $this->assertNotNull($receipt);
    }

    public function testRequestCallUnknownUser()
    {
        try {
            $receipt = $this->client->requestCall("000000");
            $this->fail();
        } catch (ResponseException $e) {
            $this->assertEquals(404, $e->getStatus());
        }
    }

    public function testApiWhenServerIsDown()
    {
        try {
            // Simulate server down
            $httpClient = new HttpClient([
                "base_uri" => "http://localhost:4444",
                "timeout" => 10.0,
            ]);
            $client = new Client($this->clientId, $this->clientSecret);
            $client->setHttpClient($httpClient);
            $user = $client->getUser("123456");
            $this->fail();
        } catch (APIConnectionException $e) {
            $this->assertNotNull($e);
        }
    }

    public function testApiWhenUsingFakeServer()
    {
        try {
            // Simulate some kind of network problem
            $httpClient = new HttpClient([
                "base_uri" => "http://fakeserver:4000",
                "timeout" => 10.0,
            ]);
            $client = new Client($this->clientId, $this->clientSecret, true);
            $client->setHttpClient($httpClient);
            $user = $client->getUser("123456");
            $this->fail();
        } catch (APIConnectionException $e) {
            $this->assertNotNull($e);
        }
    }

    public function testGetUserWithBadToken()
    {
        $client = new Client($this->clientId, $this->clientSecret, true);
        $client->setTokenStore(new \TwostepTest\BadTokenStore);
        $user = $client->getUser("123456");
        $this->assertNotNull($user);
    }

    public function testGetUserWithExpiredToken()
    {
        $client = new Client($this->clientId, $this->clientSecret, true);
        $client->setTokenStore(new \TwostepTest\ExpiredTokenStore);
        $user = $client->getUser("123456");
        $this->assertNotNull($user);
    }

    public function testError() {
        $error = new Error();
        $error->status = 400;
        $error->message = "Error messages";
        $error->success = false;
        $this->assertTrue($error->isValid());
    }

    public function testBadError() {
        $error = new Error();
        $error->status = 0;
        $error->message = "";
        $error->success = false;
        $this->assertFalse($error->isValid());
    }
}
