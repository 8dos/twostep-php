<?php

define("VERSION", "0.6.1");

//define("SANDBOX_API_URL", "https://sandbox-api.twostep.io");
define("SANDBOX_API_URL", "http://localhost:4000");
define("LIVE_API_URL", "https://api.twostep.io");

define("DEFAULT_TIMEOUT", 30.0);
