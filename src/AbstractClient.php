<?php

namespace Twostep;

require_once "Constants.php";

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Twostep\Exceptions\APIConnectionException;
use Twostep\Exceptions\APITimeoutException;
use Twostep\Exceptions\ResponseException;

abstract class AbstractClient
{
    protected $clientId;
    protected $clientSecret;
    protected $tokenStore;
    protected $http;

    public function __construct($clientId, $clientSecret, $sandbox = false, $timeout = DEFAULT_TIMEOUT)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;

        if (empty($this->clientId)) {
            throw new \InvalidArgumentException("No Client ID provided");
        }
        if (empty($this->clientSecret)) {
            throw new \InvalidArgumentException("No Client secret provided");
        }

        $this->http = new HttpClient([
            "base_uri" => ($sandbox === true ? SANDBOX_API_URL : LIVE_API_URL),
            "timeout" => $timeout,
        ]);
    }

    public function setHttpClient($httpClient)
    {
        $this->http = $httpClient;
    }

    public function setTokenStore($store)
    {
        $this->tokenStore = $store;
    }

    protected function get($path)
    {
        return $this->requestWithToken("GET", $path);
    }

    protected function post($path, $params)
    {
        return $this->requestWithToken("POST", $path, $params);
    }

    protected function put($path, $params)
    {
        return $this->requestWithToken("PUT", $path, $params);
    }

    protected function delete($path)
    {
        return $this->requestWithToken("DELETE", $path);
    }

    protected function getToken()
    {
        $token = null;
        if (!is_null($this->tokenStore)) {
            $token = $this->tokenStore->load();
        }
        if (!is_null($token) && $token->isValid()) {
            if ($token->isExpired()) {
                return $this->refreshToken($token);
            }
            return $token;
        }
        return $this->newToken();
    }

    protected function newToken()
    {
        $params = [
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "grant_type" => "client_credentials",
        ];

        $body = $this->request("POST", "/auth/token", $params);
        $token = Token::fromJSON($body);

        if (!is_null($this->tokenStore)) {
            $this->tokenStore->save($token);
        }
        return $token;
    }

    protected function refreshToken($oldToken)
    {
        $params = [
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "grant_type" => "refresh_token",
            "refresh_token" => $oldToken->refreshToken,
        ];

        $body = $this->request("POST", "/auth/token", $params);
        $token = Token::fromJSON($body);

        if (!is_null($this->tokenStore)) {
            $this->tokenStore->save($token);
        }
        return $token;
    }

    protected function requestWithToken($method, $path, $params = array())
    {
        $token = $this->getToken();
        if (is_null($token) || !$token->isValid()) {
            throw new ResponseException(401, "Authorization failed");
        }
        return $this->request($method, $path, $params, $token);
    }

    protected function request($method, $path, $params = array(), $token = null)
    {
        $headers = [
            "User-Agent" => "twostep-php/" . VERSION,
            "Accept" => "application/json",
        ];

        if (!is_null($token)) {
            $headers["Authorization"] = "Bearer " . $token->accessToken;
        }

        if (!is_null($params)) {
            $headers["Content-Type"] = "application/json";
        }

        try {
            $response = $this->http->request($method, $path, [
                "headers" => $headers,
                "json" => $params,
            ]);
            return $response->getBody();
        } catch (ConnectException $e) {
            $reason = $e->getMessage();
            $pos = strpos($reason, "cURL error 28");
            if ($pos === false) {
                throw new APIConnectionException($reason);
            } else {
                throw new APITimeoutException($reason);
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $status = $response->getStatusCode();
                $message = $e->getMessage();

                $pieces = explode("\n", $message);
                if (count($pieces) > 1) {
                    $body = $pieces[1];
                    $error = Error::fromJSON($body);
                    if ($error->isValid()) {
                        throw new ResponseException($status, $error->message);
                    } else {
                        throw new ResponseException($status, $message);
                    }
                } else {
                    throw new ResponseException($status, $message);
                }
            } else {
                throw new APIConnectionException($e->getMessage());
            }
        }
    }
}
