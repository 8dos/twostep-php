<?php

namespace Twostep;

use Lcobucci\JWT\Parser;
use MintWare\JOM\JsonField;
use MintWare\JOM\ObjectMapper;

class Token
{
    /** @JsonField(name="access_token", type="string") */
    public $accessToken;

    /** @JsonField(name="token_type", type="string") */
    public $tokenType;

    /** @JsonField(name="scope", type="string") */
    public $scope;

    /** @JsonField(name="expires_in", type="int") */
    public $expiresIn;

    /** @JsonField(name="refresh_token", type="string") */
    public $refreshToken;

    /**
     * Check if token is valid.
     *
     * @return Bool
     */
    public function isValid()
    {
        if (empty($this->accessToken) || empty($this->refreshToken)) {
            return false;
        }
        return true;
    }

    /**
     * Check if token has expired.
     *
     * @return Bool
     */
    public function isExpired()
    {
        $decoded = (new Parser())->parse($this->accessToken);
        if (!empty($decoded)) {
            $exp = (int) $decoded->getClaim("exp");
            $now = time();
            if ($exp > $now) {
                return false; // valid
            }
        }
        return true; // invalid or expired
    }

    /**
     * Create a token object from a JSON string.
     *
     * @param string $body
     * @return Token
     */
    public static function fromJSON($body)
    {
        $mapper = new ObjectMapper();
        return $mapper->mapJson($body, Token::class);
    }
}
