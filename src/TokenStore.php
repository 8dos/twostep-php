<?php

namespace Twostep;

abstract class TokenStore
{
    abstract public function load();
    abstract public function save($token);
    abstract public function clear();
}
